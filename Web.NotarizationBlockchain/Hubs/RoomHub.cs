﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NLog;
using Web.NotarizationBlockchain.Models;

namespace Web.NotarizationBlockchain.Hubs
{

    public class RoomHub : Hub
    {
        public static bool initialized = false;
        public static object initLock = new object();
        private static Logger _log = NLog.LogManager.GetCurrentClassLogger();
        public static int secs_10 = 10000;
        public RoomHub()
        {
            if (initialized)
            {
                return;
            }

            lock (initLock)
            {
                if (initialized)
                {
                    return;
                }

                InitializeAuction();
            }
        }

        private void InitializeAuction()
        {
            initialized = true;
        }
        public void CreateRoom(string name, int maxCount, string password)
        {
            var config = new RoomConfig()
            {
                ID = Guid.NewGuid().ToString().ToLower(),
                Name = name,
                MaxCount = maxCount,
                Password = password,
                CreateDateTimme = DateTime.Now.ToString(@"yyyy\/MM\/dd HH:mm:ss"),
                Roommates = new List<Roommate>()
            };

            RoomActivityHandler.RoomConfigs.TryAdd(config.ID, config);
            Clients.All.refreashRooms(RoomActivityHandler.RoomConfigs.Select(s => s.Value).ToArray());
            Clients.Caller.enterNotarizationRoom(config.ID);
        }


        public void GetCurrentRoomConfig()
        {
            Clients.Caller.refreashRooms(RoomActivityHandler.RoomConfigs.Select(s => s.Value).ToArray());
        }

        public void SendFileConfirm(string roomId, bool isConfrim)
        {
            _log.Debug($"Enter SendFileConfirm");
            _log.Debug($"Enter SendFileConfirm, roomId={roomId}");
            _log.Debug($"Enter SendFileConfirm, isConfrim={isConfrim}");
            _log.Debug($"Enter SendFileConfirm, connectionId={Context.ConnectionId}");
            if (RoomActivityHandler.RoomConfigs.ContainsKey(roomId))
            {
                var roomConfig = RoomActivityHandler.RoomConfigs[roomId];
                roomConfig.CurrentCount += 1;
                var roommate = roomConfig.Roommates.SingleOrDefault(w => w.ConnectionId == Context.ConnectionId);
                if (roommate != null)
                {
                    roommate.IsFileConfirm = isConfrim;
                    _log.Debug($"Change SendFileConfirm");
                    Clients.Group(roomId).refreashRoommate(roomConfig);
                }
            }
        }

        //public void EnterNotarizationRoom(string name, string id, string roomId)
        //{
        //    var roommate = new Roommate()
        //    {
        //        ID = id,
        //        Name = name,
        //        ConnectionId = Context.ConnectionId
        //    };
        //    _log.Debug($"EnterNotarizationRoom, connectionID = {Context.ConnectionId}");
        //    if (RoomActivityHandler.RoomConfigs.ContainsKey(roomId))
        //    {
        //        var roomConfig = RoomActivityHandler.RoomConfigs[roomId];
        //        roomConfig.CurrentCount += 1;
        //        roomConfig.Roommates.Add(roommate);
        //    }

        //    Clients.All.refreashRooms(RoomActivityHandler.RoomConfigs.Select(s => s.Value).ToArray());
        //}


        public override Task OnConnected()
        {
            var type = Context.QueryString["type"];
            _log.Debug($"type = {type}");
            if (type != null && type == "notarizationRoom")
            {
                UserRepository repo = new UserRepository();
                var roomId = Context.QueryString["roomId"];
                var userSN = Context.QueryString["userSN"];
                var user = repo.GetNotaryPublic(Int32.Parse(userSN));
                _log.Debug($"roomId = {roomId}");
            
                var roommate = new Roommate()
                {
                    UserSN = Int32.Parse(userSN),
                    Name = user.Name,
                    ConnectionId = Context.ConnectionId,
                    Company = user.Company,
                    EnterDateTime = DateTime.Now,
                    Headshot = user.Headshot,
                    Title = user.Title,
                    UserType = user.UserType
                };

                if (RoomActivityHandler.RoomConfigs.ContainsKey(roomId))
                {
                    var roomConfig = RoomActivityHandler.RoomConfigs[roomId];
                    roomConfig.CurrentCount += 1;
                    roomConfig.Roommates.Add(roommate);
                    Groups.Add(Context.ConnectionId, roomId);
                    Clients.Group(roomId).refreashRoommate(roomConfig);
                    Clients.Caller.refreashRoommate(roomConfig);
                }

                Clients.All.refreashRooms(RoomActivityHandler.RoomConfigs.Select(s => s.Value).ToArray());
            }
            _log.Debug($"OnConnected, connectionID = {Context.ConnectionId}");
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var pair = RoomActivityHandler.RoomConfigs.Where(w => w.Value.Roommates.Any(a => a.ConnectionId == Context.ConnectionId));

            if (pair != null && pair.Any())
            {
                lock (initLock)
                {
                    var roomConfig = pair.SingleOrDefault().Value;
                    var roommate = roomConfig.Roommates.SingleOrDefault(s => s.ConnectionId == Context.ConnectionId);
                    if (roommate != null)
                    {
                        _log.Debug($"Roommates.Remove");
                        roomConfig.Roommates.Remove(roommate);
                        roomConfig.CurrentCount -= 1;
                        Clients.All.refreashRooms(RoomActivityHandler.RoomConfigs.Select(s => s.Value).ToArray());
                    }
                }

            }

            _log.Debug($"OnDisconnected, connectionID = {Context.ConnectionId}");
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
    }

    public static class RoomActivityHandler
    {
        public static ConcurrentDictionary<string, RoomConfig> RoomConfigs = new ConcurrentDictionary<string, RoomConfig>();
    }

    public class RoomConfig
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int MaxCount { get; set; }
        public int CurrentCount { get; set; }
        public string Password { get; set; }
        public string CreateDateTimme { get; set; }
        public List<Roommate> Roommates { get; set; }
    }

    public class Roommate
    {
        public string ConnectionId { get; set; }
        public int UserSN { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public string Headshot { get; set; }
        public string UserType { get; set; }
        public DateTime EnterDateTime { get; set; }
        public bool IsFileConfirm { get; set; }
    }
}