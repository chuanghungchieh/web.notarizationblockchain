﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.NotarizationBlockchain.Models
{
    public class UserRepository
    {

        public NotaryPublic GetNotaryPublic(int userSN)
        {
            NotarizationDataContext dc = new NotarizationDataContext();
            return dc.NotaryPublic.SingleOrDefault(w => w.SN == userSN);
        }
    }
}