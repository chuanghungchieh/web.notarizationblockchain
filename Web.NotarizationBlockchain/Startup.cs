﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup(typeof(Web.NotarizationBlockchain.Startup))]
namespace Web.NotarizationBlockchain
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            //SignalR 設定
            var config = new HubConfiguration();
            config.EnableJSONP = true;
            //config.EnableCrossDomain = true;
            app.MapSignalR(config);
        }
    }
}