﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.NotarizationBlockchain.Hubs;

namespace Web.NotarizationBlockchain.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult NotarizationRoom(string roomId)
        {
            var model = RoomActivityHandler.RoomConfigs.FirstOrDefault(w => w.Key == roomId).Value;
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}